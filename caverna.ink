
TODO: Ver como se incluyen los distintos ficheros para juntarlos.

No podía ver nada en aquella impenetrable oscuridad. Notaba como me llevaban en alguna especie de camilla, pero era imposible saber en que dirección nos movíamos.
El silencio con el que aquella gente se desenvolvía era perturbador. Más allá de algún roce de telas ocasional el único sonido que había en aquella ¿caverna?, era mi respiración entrecortada. Incluso parecía que se comunicaran telepáticamente puesto que los giros en el camino, levantar la camilla para esquivar algún obstáculo invisible o simplemente mantenerla firme mientras nos internábamos más y más en las sombreas, no requerían de ningún comentario por parte de mis mudos captores.
Pensé en hablar o proferir algún sonido con tal de romper el agobiante silencio
* Pero [preferí no hacerlo.] supuse que sólo serviría para empeorar mi situación, así que me mantuve en el máximo silencio de que era capaz mientras buscaba en mi corazón imágenes que me hicieran mantener el poco ánimo que aún conservaba. -> arrastrado_por_la_caverna
* Y cuando reuní ánimo suficiente para hacerlo[], descubrí que lo que fuera que me habían dado había paralizado mis cuerdas vocales, o al menos los músculos que las gobernaban, pues no pude proferir sonido alguno. -> arrastrado_por_la_caverna

== arrastrado_por_la_caverna ==
Aunque temía el momento en que nuestro viaje acabase,
* un cierto deseo de que hubiera algún cambio[...], me hacía esperar cada giro del camino con ansiedad, deseando que fuese el último.
* pensé que cualquier cosa sería mejor que aquella incertidumbre[...], esperando a cada momento un golpe o algo peor.
* mi mente volaba lejos de allí[...] y pensaba si todo acabaría valiendo la pena.
- Cuando al fin mis captores se detuvieron y sentí que habíamos llegado al final del camino, un olor muy fuerte llegó a mis fosas nasales y todo se desvaneció.
-> primer_interrogatorio

== primer_interrogatorio ==
Cuando recobré el conocimiento, la oscuridad había dado paso a una luz fría y enceguecedora.
Estaba en una habitación pequeña, boca arriba sobre lo que parecía un camastro, pero que pronto deduje que debía ser algún tipo de aparato de tortura. Mis manos y pies estaban atados con una cuerda gruesa y tensa que me mantenía en tensión, con la espalda apenas rozando la superficie del artilugio. Estirándome un poco pude ver un mecanismo de poleas cerca del borde inferior y no cabían muchas dudas sobre su utilidad.
Iban a sacarme la confesión mediante el tormento.
TODO: Inventar un nombre mejor para los inquisidores.
Había oído muchas historias acerca de los torturadores de los Discípulos (que preferían llamarse a sí mismos, Escrutadores). Si no conseguían la verdad, el desdichado se convertía en un ejemplo para el siguiente que se negarse a colaborar. Verdad o Muerte era su credo y eran muy efectivos en hacerlo cumplir.
Intenté mirar alrededor para ver que me esperaba, pero en la postura en la que estaba sólo podía ver la silla del Escrutador, a los pies de aquél artilugio y muy cerca del mecanismo que conseguía erizarme el vello sólo al contemplarlo.
* Después de lo que parecieron horas[], mis sentidos parecieron entumecerse. Me parecía oir cosas amortiguadas en la distancia. Movimientos. Posiblemente mi imaginación jugándome una mala pasada.
* La pierna me dolía aún más[]. La postura me forzaba la articulación dañada y cada respiración era más dolorosa que la anterior.
* Intenté imaginarme libre[], pero mis pensamientos iban una y otra vez al mismo sitio. Alguien me necesitaba. Necesitaba mi lealtad y toda la fuerza de voluntad que consiguiese reunir. Y no podía fallarle.
- En aquél momento se abrió la puerta y oí que entraba gente en la habitación. Debido a mi forzada postura, no pude ver su aspecto.







-> END