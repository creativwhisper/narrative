// Como pisadas en la arena

// Cuatro puntos cardinales y el deseo de exploración
// ¿Cómo forja la exploración el carácter del individuo?
// Importancia de la meditación y la introspección para encontrar el verdadero camino.
// La respuesta no importa, importa el camino y su interpretación

// No se yo si la segunda persona es la más apropiada para escribir esto...

VAR viajando_por_el_desierto = 0

TODO: Estructura. Dividir en actos y cada acto con nodos principales, el resto de nodos cuelgan de ellos. Si encuentro la manera, usar include-nodo principal-trama dentro del nodo.

-> inicio
== inicio ==
Cuando abríste los ojos, y se aclimataron al reflejo del sol sobre la arena del desierto, lo primero que llamó tu atención fue -> preguntas_iniciales
= preguntas_iniciales
* [El enorme cartel que tapaba el sol] el enorme cartel que se alzaba desafiante en mitad del desierto y que proyectaba la única sombra que había en muchos kilómetros a la redonda. -> mirando_el_cartel
* La chica que miraba el cielo -> mirando_a_la_chica
* [El anciano sentado sobre la duna] -> mirando_al_anciano
*  -> final.fracaso

== mirando_el_cartel
El mástil de aquél mastodonte se elevaba al menos una veintena de metros y estaba fabricado en una madera oscura que ya se había astillado por varios puntos dando fe de su extrema antigüedad. Lo coronaba un panel en el que se distinguían unas letras, pero eran difíciles de leer desde donde te encontrabas. -> preguntas_mirando_el_cartel
== preguntas_mirando_el_cartel
* [Por qué] Te preguntaste cuál sería su propósito. ¿Habrían intentado crear un punto de referencia en aquél inhóspito lugar? Si ese era el caso, ¿por qué no había más hitos en los alrededores que orientasen al viajero? ¿Qué podría estar señalizando aquél enorme poste en mitad de ninguna parte? -> preguntas_mirando_el_cartel
* [Quién] Intentaste imaginarte a quién fuera que hubiera llevado aquél enorme poste hasta allí y cómo lo habría hecho. Ningún camino llevaba hasta aquél punto ni había nada en los alrededores que hubiera podido servir de materia prima. ¿Fué una sola persona? ¿Fueron varias? No eran más que preguntas sin solución. -> preguntas_mirando_el_cartel
* [Qué había escrito] -> ver_lo_que_hay_escrito  
* [El cartel no te interesaba] Pero el cartel no te interesaba, así que te alejaste de él. -> inicio.preguntas_iniciales 

= ver_lo_que_hay_escrito
Comenzaste a caminar alejándote del poste, con la intención de ver que había escrito en el panel de su cúspide. El sol estaba a su espalda y te deslumbraba demasiado. Tuviste que alejarte muchos pasos, con la arena cediendo bajo tus pies y haciéndote tropezar constantemente. Finalmente, estuviste lo suficientemente lejos como para leer lo que coronaba aquél poste. -> nada 

= nada
* "NADA"[]

En grandes letras rojas destacadas sobre un fondo blanco, esa simple palabra era todo lo que decoraba el cartel. Una bienvenida a ninguna parte. Fuera lo que fuese lo que impulsase a alguien a instalar allí aquella fútil proclama, nunca podrías saberlo.
-> vuelta_al_desierto


== mirando_a_la_chica
TODO: Escribir esta parte. Ella es el viento que borra los rastros.
<> de espaldas a tí. Era una niña, alta y delgada, y parecía mirar muy fijamente algo distante sobre el horizonte.
* [Miraste a donde ella miraba]Acercándote hasta ponerte a su lado, dirigiste tu mirada hacia donde ella miraba.
En un primer momento te pareció que no había nada, pero cuando tus ojos se acostrumbraron a mirar en la distancia, vislumbraste las formas difusas de algunas aves.
"Me gusta verlas jugar" dijo la chica sobresaltándote a tu pesar. "Casi puedo sentir sus plumas sobre mi piel al verlas aletear."
En aquél momento te parecía que ella podría responder a cualquier pregunta.
** [Permaneciste en silencio]
** [Le preguntáste donde te encontrabas]
** [Le preguntáste quién era]
-> vuelta_al_desierto
* Le hablaste[]
* No te interesaba la chica particularmente[] <> así que la dejaste con sus tribulaciones. -> inicio.preguntas_iniciales

-> END

== mirando_al_anciano
el anciano que estaba sentado completamente inmóvil sobre la arena, con los ojos cerrados. Tenía las piernas cruzadas formando la posición del loto y sus brazos caían inermes a sus lados, con las manos hundidas en la arena.
Parecía irradiar una extraña paz.
* Le observaste[], intentando no perturbar lo que fuese que estuviera haciendo. Era muy anciano y su negra piel estaba surcada de arrugas profundas. La barba que enmarcaba su rostro era rala, pero de un blanco casi resplandeciente. Iba vestido sólo con un taparrabos parduzco.
Al poco de estar observándole reparaste en que lo que en un principio habías tomado por lunares sobre su piel, eran en realidad pequeños tatuajes con forma de estrella. Tenía el cuerpo plagado de pequeñas constelaciones.
Su presencia en aquél lugar te llenaba de curiosidad,
** [Así que le hablaste] lo mismo estaba allí esperándote para comunicarte algo. O simplemente esperaba que reparases su presencia, así que te animaste a romper el silencio que había a vuestro alrededor. -> hablando_con_el_anciano
** [Pero seguiste observandole] pero no te atreviste a romper su meditación y seguiste observándole con tranquilidad. -> el_angel_de_arena
* [Le hablaste] Cuando estabas a punto de preguntarle si podía ayudarte -> hablando_con_el_anciano
* Te sentaste a su lado[], -> sentado_con_el_anciano
* [El anciano no te interesaba] No te interesaba particularmente aquél extraño anciano, así que decidiste no molestarle. -> inicio.preguntas_iniciales
-> END

= hablando_con_el_anciano
Súbitamente, el hombre alzó sus manos en un rápido y fluído movimiento, levantando sendos arcos de arena a sus lados mientras el sol resplandecía a su espalda.
Durante un fugaz momento, apenas un suspiro, pareció un ángel extendiendo sus doradas alas al sol del atardecer. En un parpadeo, el hombre se había desvanecido sin dejar rastro ni huella alguna sobre la arena.
-> vuelta_al_desierto

= sentado_con_el_anciano
pues la presencia de aquél hombre generaba tanta paz que sentiste que debías experimentar el mundo desde su perspectiva. Con dificultad, te sentaste frente a él con las piernas cruzadas y cerraste los ojos, intentando vaciar la mente, como se solía sugerir para inducir la meditación.
* Esperaste[].
El tiempo transcurría lentamente y el silencio era impenetrable, sin embargo te asaltaban todo tipo de distracciones: el calor del intenso sol, un pequeño insecto trepándo por tu brazo, el roce de la ropa en tu cuerpo. Todo parecía entorpecer tus tímidos esfuerzos.
** Esperaste[].
Poco a poco empezaste a integrar las distracciones en la meditación y tu cuerpo se relajó. Comenzaste a sentir como el desierto te llamaba, incitándote a perderte en él, dejar vagar la consciencia por sus dunas y olvidarlo todo.
*** Esperaste[].
No había nada. Sólo existíais el desierto y tú, un tú cada vez más insignificante ante la inmensidad de aquella nueva consciencia. Aquél lugar era todo el universo, y tú, apenas un grano de arena. Comprendiste súbitamente que, en el imperturbable paso de los milenios, las tímidas ondulaciones de tus actos desaparecerían sin dejar rastro, como las de toda la humanidad. El ser humano es insignificante para el Tiempo.
TODO: No me gusta este último párrafo, repasar durante la revisión final.
**** Esperaste[].
De repente, el anciano se abrió paso en tu mente y parecía llenar toda tu consciencia. En su rostro había una tranquila sonrisa, la propia de alguien que ha alcanzado un nivel de paz al alcance de muy pocos. Cuando el anciano de tu mente abrió los ojos, viste en ellos satisfacción por tu comprensión.
***** No sabes cuanto tiempo había transcurrido[], pero tu consciencia finalmente dejó atrás la meditación y abriste los ojos. -> vuelta_al_desierto
-> END

= el_angel_de_arena
No sabrías decir cuanto tiempo estuviste en aquella contemplación, pero te diste cuenta de que el hombre no estaba en silencio. De manera casi imperceptible movía la boca en lo que dedujiste que se trataba de un rezo o cántico.
Ahora que prestabas atención oías claramente el murmullo que salía de sus, a pesar de todo, casi inmóviles labios. No podías entender las palabras, que sonaban guturales y extrañas, pero su cadencia te serenaba y aclaraba la mente. Las estrellas sobre su piel parecían danzar y moverse como si las cuatro estaciones estuvieran ocurriendo sucesivamente.
Sentiste que era un cántico dedicado a las mismas estrellas, invisibles a aquella hora del día.
* Repentinamente[], el hombre alzó sus manos en un rápido y fluído movimiento, levantando sendos arcos de arena a sus lados mientras el sol resplandecía a su espalda.
Durante un fugaz momento, apenas un suspiro, pareció un ángel extendiendo sus doradas alas al sol del atardecer.
** En un parpadeo[], el hombre se había desvanecido sin dejar rastro ni huella alguna sobre la arena. Pero al mirar el cielo, que había empezado a tornarse violáceo ante la llegada de la noche, te pareció ver el titilar de una única estrella. -> vuelta_al_desierto
-> END

== vuelta_al_desierto
Cuando volviste a mirar a tu alrededor, reparaste en que tanto la chica como el anciano habían desaparecido. Ningún sonido había llegado a tus oidos ni habías llegado a percibir ningún movimiento en aquél desolado paraje. Sin embargo, no estaban allí. Miraste a tu alrededor buscando algún punto de referencia que no fuese el enorme poste, pero no había nada. Te habías quedado completamente sólo y sin guía en un lugar desconocido.
* Esperaste...[]... un tiempo en aquél lugar. ¿Pasaría alguien por allí en algún momento? 
* Decidiste que no tenía sentido esperar[. ] y que lo mejor sería caminar en alguna dirección, esperando encontrar alguien o algo que pudiera orientarte. -> perdido
-> END

== perdido ==
Estabas en mitad de ninguna parte, sólo con el mortecino sol sirviéndote de brújula. Decidiste continuar tu camino.
* [Hacia el norte] -> norte("norte")
* [Hacia el sur] -> sur
* [Hacia el este] -> este
* [Hacia el oeste] -> oeste

= norte(direccion)
// hacemos pasar al jugador tres veces por la decisión y luego le mostramos la salida. Si no la coge, el juego le lleva directamente al final del desierto.
~viajando_por_el_desierto++
{Comenzaste a caminar hacia el {direccion} donde las dunas| Continuaste tu camino hacia el {direccion} mientras las dunas} {~que se extendían hasta donde alcanzaba la vista|parecían allanarse|resplandecían a la luz del sol}, {~aunque no servían para encontrar el camino a ninguna parte|que permanecían imperturbables ante tus tribulaciones|sin el menor rastro humano en kilómetros a la redonda}. {~Algunas malezas aparecían esparcidas en las zonas menos expuestas al viento|Sólo unos pocos pájaros en el horizonte, demasiado altos para ver su especie|El ocaso amenazaba con alcanzarte mientras caminabas por aquél yermo|El silencio sólo era roto por el susurro del viento sobre la arena}.
{!El cielo comenzaba a oscurecerse mientras la noche caía lentamente sobre el desierto.|Sobre tu cabeza comenzaban a vislumbrarse las estrellas, pero no viste ninguna constelación conocida con la que poder orientarte.|Noche cerrada, un cielo cuajado de estrellas frías y una brisa seca que trae aromas de soledad.}
{~Tus fatigados pies se esforzaron para continuar.|Intentaste continuar con resolución.|No te dejaste intimidar por la inmensidad y continuaste andando.|Poco parecía importar hacia donde continuar, pero aún así, analizaste tus opciones antes de proseguir tu camino.}


{ viajando_por_el_desierto > 3: -> fin_del_camino }
* { viajando_por_el_desierto > 2 } Algo capta tu atención[] -> oasis
+ [Hacia el norte] -> norte("norte")
+ [Hacia el sur] -> norte("sur")
+ [Hacia el este] -> norte("este")
+ [Hacia el oeste] -> norte("oeste")

= sur
~viajando_por_el_desierto++
Dunas {~que se extendían hasta donde alcanzaba la vista|parecían allanarse|resplandecían a la luz del sol}, {~que no servían para encontrar el camino a ninguna parte|que permanecían imperturbables ante tus tribulaciones|sin el menor rastro humano en kilómetros a la redonda}. {~Algunas malezas aparecían esparcidas en las zonas menos expuestas al viento|Sólo unos pocos pájaros en el horizonte, demasiado altos para ver su especie|El ocaso amenazaba con alcanzarte mientras caminabas por aquél yermo|El silencio sólo era roto por el susurro del viento sobre la arena}.

{~Tus fatigados pies se esforzaron para continuar|Intentaste continuar con resolución|No te dejaste intimidar por la inmensidad y continuaste|Poco parecía importar hacia donde continuar, pero aún así, analizaste tus opciones antes de dirigir tus siguientes pasos}...
{ viajando_por_el_desierto > 3: -> fin_del_camino }
* { viajando_por_el_desierto > 2 } Algo capta tu atención[] -> lucha_de_escorpiones
+ Hacia el norte[] -> sur
+ Hacia el sur[] -> sur
+ Hacia el este[] -> sur
+ Hacia el oeste[] -> sur


= este
~viajando_por_el_desierto++
Dunas {~que se extendían hasta donde alcanzaba la vista|parecían allanarse|resplandecían a la luz del sol}, {~que no servían para encontrar el camino a ninguna parte|que permanecían imperturbables ante tus tribulaciones|sin el menor rastro humano en kilómetros a la redonda}. {~Algunas malezas aparecían esparcidas en las zonas menos expuestas al viento|Sólo unos pocos pájaros en el horizonte, demasiado altos para ver su especie|El ocaso amenazaba con alcanzarte mientras caminabas por aquél yermo|El silencio sólo era roto por el susurro del viento sobre la arena}.

{~Tus fatigados pies se esforzaron para continuar|Intentaste continuar con resolución|No te dejaste intimidar por la inmensidad y continuaste|Poco parecía importar hacia donde continuar, pero aún así, analizaste tus opciones antes de dirigir tus siguientes pasos}...
{ viajando_por_el_desierto > 3: -> fin_del_camino }
* { viajando_por_el_desierto > 2 } Algo capta tu atención[] -> espejismos
+ Hacia el norte[] -> este
+ Hacia el sur[] -> este
+ Hacia el este[] -> este
+ Hacia el oeste[] -> este


= oeste

TODO: ¿Qué pongo en este?
~viajando_por_el_desierto++
Dunas {~que se extendían hasta donde alcanzaba la vista|parecían allanarse|resplandecían a la luz del sol}, {~que no servían para encontrar el camino a ninguna parte|que permanecían imperturbables ante tus tribulaciones|sin el menor rastro humano en kilómetros a la redonda}. {~Algunas malezas aparecían esparcidas en las zonas menos expuestas al viento|Sólo unos pocos pájaros en el horizonte, demasiado altos para ver su especie|El ocaso amenazaba con alcanzarte mientras caminabas por aquél yermo|El silencio sólo era roto por el susurro del viento sobre la arena}.

{~Tus fatigados pies se esforzaron para continuar|Intentaste continuar con resolución|No te dejaste intimidar por la inmensidad y continuaste|Poco parecía importar hacia donde continuar, pero aún así, analizaste tus opciones antes de dirigir tus siguientes pasos}...
{ viajando_por_el_desierto > 3: -> fin_del_camino }
* { viajando_por_el_desierto > 2 } Algo capta tu atención[] -> espejismos
+ Hacia el norte[] -> oeste
+ Hacia el sur[] -> oeste   
+ Hacia el este[] -> oeste
+ Hacia el oeste[] -> oeste
-> oeste

-> END

== oasis

-> END

== lucha_de_escorpiones
En el suelo, dos escorpiones casi del tamaño de tu mano estaban luchando en el lado umbrío de una duna.
Uno de ellos, el más pequeño, parecía mal herido. Una de sus pinzas estaba lánguida arrastrando por el suelo y se defendía sólo con la otra. El otro mantenía su ofensiva, pinchando al pequeño con su aguijón y acorralándole contra una piedra.
Era evidente que la lucha no duraría mucho más. El pequeño estaba condenado.
* [Observar] Permaneciste observando el injusto combate sin intervenir. Poco a poco el escorpión más grande había logrado acorralar a su contrincante y le había arrancado la pinza que aún tenía sana.
El pequeño estaba ya inmóvil y prácticamente muerto cuando, en un súbito movimiento, su cola descargó un poderoso latigazo sobre el cuerpo de su agresor, clavándose profundamente en su carne. Apenas un segundo después, falleció.
El escorpión victorioso agonizaba ante tus ojos, su cuerpo sufriendo violentos espásmos, pero con el aguijón de su adversario aún profundamente clavado y sin poder arrancárselo.
Apenas unos minutos después su cuerpo quedó inerte frente a tí, en aquél abrazo mortal que había unido al vencedor y al vencido.
* [Continuar] La lucha de dos pequeños artrópodos no significaba nada para tí y menos cuando el resultado era tan evidente, así que continuaste tu camino sin mirar atrás.
* [Ayudar al pequeño]La compasión ante una lucha tan desigualada te pudo y decidiste echarle una mano al más pequeño. Así que cogiste una roca cercana y en un movimiento rápido para evitar la respuesta del escorpión, machacáste al más grande golpeándolo con ella violéntamente. Todo fue rápido.
El más pequeño pareció sorprendido ante la súbita desaparición de su adversario y retrocedió ligeramente para luego acercarse de nuevo hasta tu posición. Su desconcierto era evidente.
Lo que no evitó que con un movimiento aún más rápido que el tuyo te clavase el aguijón en la pierna.
El dolor te hizo maldecir a voz en grito, pero aunque buscaste a tu ingrato agresor, no fuiste capaz de encontrarlo. Tan pronto había hundido el aguijón en tu carne había huido sin dejar rastro.
Realmente la herida no te suponía más que un leve escozor, pero la marca en tu orgullo había sido mucho más profunda.
Continuaste tu camino intentando olvidar lo que había ocurrido.
* [Aplastar el escorpión pequeño]Queriendo ahorrarte el sufrimiento de ver agonizar a aquella pequeña alimaña, agarraste una roca cercana y machacaste el escorpión agonizante.
El más grande se removió nervioso y, posiblemente, sorprendido de ver como su rival desaparecía súbitamente. Daba vueltas alrededor de la piedra intentando encontrar el cuerpo o al menos parte de sus restos.
Girando la piedra, dejáste el cadaver expuesto y el escorpión no dudó en lanzarse sobre su presa y comenzar a devorarla. El hambre de aquella criatura era evidente.
Sin embargo si esperabas algún agradecimiento por su parte, era evidente que no obtendrías ninguno.
Alejándote de aquél festín improvisado, decidiste continuar tu camino.
* [Aplastarlos a ambos] Cogiste una piedra cercana y, en lo más álgido de la pelea, la descargaste sobre ambos contendientes. El crujido de sus cuerpos sonó algo obsceno en el silencio del desierto.
Contemplaste ambos cuerpos sin ningún atisbo de compasión. Seguramente había alguna lección encerrada en aquella acción, pero en aquél momento sólo pensaste en que habías hecho justicia al terminar con el sufrimiento de uno y acabar con la violencia del otro.
Dejando caer la piedra con un ruido sordo, proseguiste tu camino.
- -> fin_del_camino

-> END

== espejismos

-> END

== fin_del_camino ==
Saliendo del desierto.
-> nomada

== nomada ==
TODO: En esta parte un nómada hace un resumen de la filosofía del desierto y se ofrece a llevar al protagonista a casa. (Una casa metafórica puesto que está soñando)
-> final

== final ==
Abriste tu diario pues tenías que intentar plasmar tus últimas vivencias. Dejáste a la pluma bailar por el papel mientras reflexionabas sobre...->END
* { preguntas_mirando_el_cartel.ver_lo_que_hay_escrito } [...el significado de las empresas humanas.] Recordando aquél cartel en medio de la nada pensé en como las acciones pueden tener significado, aunque no se lo encontremos.
* { mirando_al_anciano.el_angel_de_arena } [...la contemplación y la meditación.] Recordar la paz que irradiaba aquél anciano y como su mera contemplación me hacía llegar a pensamientos elevados.

Escribiste mucho aquél día, sabiendo que el relato era inconexo y enrevesado. Posiblemente nadie podría encontrarle sentido a todas aquellas anotaciones.
Al terminar, releiste todo y te diste cuenta de que no era más que una historia, cómo las que sin duda habría miles en el mundo y muchas más que habrían de surgir en el futuro.
** Pero aquella era la tuya.
Finalmente, cerraste el diario con delicadeza, apagaste la vela con un soplido, y te dirigiste a la cama.
-> DONE


= fracaso
// Si al jugador no le interesa nada, hagámosle ver que la apatía no lleva a ningún sitio.
Finalmente todo a tu alrededor se había desvanecido y caíste en un profundo sopor.
* Despertaste en tu cama[] en la hora fría que precede al alba. En la oscuridad de tu habitación te cuestionaste sobre la naturaleza de los sueños. ¿Qué juegos de la mente consiguen provocar alucinaciones tan vívidas? Y más aún, dado que la mente las producía sin esperar nada a cambio ¿Qué ganábamos no explorándolas?
Posiblemente podrías haber mostrado algo de curiosidad por lo que tu inconsciente quería mostrarte, pero en aquél momento se te antojaba absurda aquella idea.
No pensaste que fueses a poder conciliar el sueño de nuevo, así que apartaste la sábanas y te preparaste para afrontar un nuevo día.

FIN
-> DONE

















